use std::time::Duration;

use failure::{err_msg, Fallible, ResultExt};
use rusb;

pub struct Device {
    dev: rusb::DeviceHandle<rusb::GlobalContext>,
}

impl Device {
    pub fn open() -> Fallible<Option<Self>> {
        if let Some(dev) = rusb::open_device_with_vid_pid(0x16C0, 0x0478) {
            dev.claim_interface(0).context("Claim USB interface")?;
            Ok(Self { dev })
        } else {
            Ok(None)
        }

        let mut dev = dev.unwrap(); // This unwrap will not panic due to the wait loop above
                                    // this is ifdefed out in original code
                                    // if dev.kernel_driver_active(0)? {
                                    //     dev.detach_kernel_driver(0)?;
                                    // }
    }

    pub fn write_block(block: &[u8], timeout: Duration) -> Fallible<usize> {
        Ok(self
            .dev
            .write_control(0x21, 9, 0x200, 0, block, timeout)
            .context("Writing data block")?)
    }
}
