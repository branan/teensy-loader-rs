use std::time::{Duration, Instant};

use core::ffi::c_void;
use core_foundation::{
    base::{kCFAllocatorDefault, CFRelease, FromVoid},
    number::CFNumber,
    runloop::{
        kCFRunLoopDefaultMode, kCFRunLoopRunHandledSource, CFRunLoopGetCurrent, CFRunLoopRunInMode,
    },
    string::{kCFStringEncodingUTF8, CFStringCreateWithCString},
};
use failure::{err_msg, Fallible};
use io_kit_sys::{
    hid::{
        base::IOHIDDeviceRef,
        device::{IOHIDDeviceClose, IOHIDDeviceGetProperty, IOHIDDeviceOpen, IOHIDDeviceSetReport},
        keys::{
            kIOHIDOptionsTypeNone, kIOHIDProductIDKey, kIOHIDReportTypeOutput, kIOHIDVendorIDKey,
        },
        manager::{
            IOHIDManagerClose, IOHIDManagerCreate, IOHIDManagerOpen, IOHIDManagerRef,
            IOHIDManagerRegisterDeviceMatchingCallback, IOHIDManagerRegisterDeviceRemovalCallback,
            IOHIDManagerScheduleWithRunLoop, IOHIDManagerSetDeviceMatching,
            IOHIDManagerUnscheduleFromRunLoop,
        },
    },
    ret::{kIOReturnSuccess, IOReturn},
};
use log::trace;
use std::sync::{Arc, Mutex};

pub struct Device {
    _manager: Manager,
    dev: IOHIDDeviceRef,
}

impl Device {
    pub fn open() -> Fallible<Option<Self>> {
        unsafe {
            let manager = Manager::open()?;

            let devices = match manager.devices.lock() {
                Ok(devices) => devices,
                Err(_) => {
                    return Err(err_msg(
                        "Could not open IOHIDManager due to a poisoned mutex",
                    ))
                }
            };

            let vid_key = CFStringCreateWithCString(
                kCFAllocatorDefault,
                kIOHIDVendorIDKey,
                kCFStringEncodingUTF8,
            );
            let pid_key = CFStringCreateWithCString(
                kCFAllocatorDefault,
                kIOHIDProductIDKey,
                kCFStringEncodingUTF8,
            );
            if let Some(dev) = devices
                .iter()
                .find(|dev| {
                    let vid = IOHIDDeviceGetProperty(dev.0, vid_key);
                    if vid == core::ptr::null_mut() {
                        return false;
                    }
                    let vid = match CFNumber::from_void(vid).to_i32() {
                        Some(vid) => vid,
                        None => return false,
                    };

                    let pid = IOHIDDeviceGetProperty(dev.0, pid_key);
                    if pid == core::ptr::null_mut() {
                        return false;
                    }
                    let pid = match CFNumber::from_void(pid).to_i32() {
                        Some(pid) => pid,
                        None => return false,
                    };

                    trace!("Examining device {:04X}:{:04X}", vid, pid);
                    (vid, pid) == (0x16C0, 0x0478)
                })
                .cloned()
            {
                let res = IOHIDDeviceOpen(dev.0, kIOHIDOptionsTypeNone);
                if res != kIOReturnSuccess {
                    return Err(err_msg("Could not open IOHIDDevice"));
                }

                drop(devices); // Necessary to free the borrow on `manager` so we can reutrn it here.
                Ok(Some(Self {
                    _manager: manager,
                    dev: dev.0,
                }))
            } else {
                Ok(None)
            }
        }
    }

    pub fn write_block(&mut self, block: &[u8], timeout: Duration) -> Fallible<usize> {
        let start = Instant::now();
        while start.elapsed() < timeout {
            let result = unsafe {
                let len = block.len() as _;
                let ptr = block as *const [u8] as *const u8;
                IOHIDDeviceSetReport(self.dev, kIOHIDReportTypeOutput, 0, ptr, len)
            };
            if result == kIOReturnSuccess {
                return Ok(block.len());
            }
        }
        Err(err_msg("Write to IOHIDDevice timed out"))
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe {
            IOHIDDeviceClose(self.dev, kIOHIDOptionsTypeNone);
        }
    }
}

// Pointers aren't send, so for our callbacks to play nice we need to
// wrap the raw pointer type in our own that we can impl Send for.
#[derive(Clone)]
struct IOHIDDevice(IOHIDDeviceRef);

unsafe impl Send for IOHIDDevice {}

struct Manager {
    devices: Arc<Mutex<Vec<IOHIDDevice>>>,
    mgr: IOHIDManagerRef,
}

impl Manager {
    fn open() -> Fallible<Self> {
        unsafe {
            let mgr = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
            if mgr == core::ptr::null_mut() {
                return Err(err_msg("Could not open IOHIDManager"));
            }

            IOHIDManagerSetDeviceMatching(mgr, core::ptr::null());
            IOHIDManagerScheduleWithRunLoop(mgr, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);

            let devices = Arc::new(Mutex::new(Vec::new()));
            {
                let context = Arc::into_raw(devices.clone()) as *mut _;
                IOHIDManagerRegisterDeviceMatchingCallback(mgr, hid_attach_device, context);

                let context = Arc::into_raw(devices.clone()) as *mut _;
                IOHIDManagerRegisterDeviceRemovalCallback(mgr, hid_remove_device, context);
            }

            let res = IOHIDManagerOpen(mgr, kIOHIDOptionsTypeNone);
            if res != kIOReturnSuccess {
                IOHIDManagerUnscheduleFromRunLoop(
                    mgr,
                    CFRunLoopGetCurrent(),
                    kCFRunLoopDefaultMode,
                );
                CFRelease(mgr as *const _);
                return Err(err_msg("Could not open IOHIDManager"));
            }

            while CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0.0, 1) == kCFRunLoopRunHandledSource {}
            Ok(Self { devices, mgr })
        }
    }
}

impl Drop for Manager {
    fn drop(&mut self) {
        unsafe {
            IOHIDManagerClose(self.mgr, kIOHIDOptionsTypeNone);
            CFRelease(self.mgr as *const _);
        }
    }
}

unsafe extern "C" fn hid_attach_device(
    context: *mut c_void,
    _result: IOReturn,
    _sender: *mut c_void,
    device: IOHIDDeviceRef,
) {
    if let Ok(mut devices) = (*(context as *const Mutex<Vec<IOHIDDevice>>)).lock() {
        devices.push(IOHIDDevice(device));
    }
}

unsafe extern "C" fn hid_remove_device(
    context: *mut c_void,
    _result: IOReturn,
    _sender: *mut c_void,
    device: IOHIDDeviceRef,
) {
    if let Ok(mut devices) = (*(context as *const Mutex<Vec<IOHIDDevice>>)).lock() {
        if let Some(idx) =
            devices
                .iter()
                .enumerate()
                .find_map(|(i, d)| if d.0 == device { Some(i) } else { None })
        {
            devices.swap_remove(idx);
        }
    }
}
