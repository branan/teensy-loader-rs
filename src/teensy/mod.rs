use std::fmt;
use std::mem::size_of;
use std::time::Duration;

use failure::{bail, err_msg, Fallible};
use log::trace;

#[cfg(not(target_os = "macos"))]
mod rusb;

#[cfg(target_os = "macos")]
mod iokit;

mod sys {
    #[cfg(not(target_os = "macos"))]
    pub use super::rusb::*;

    #[cfg(target_os = "macos")]
    pub use super::iokit::*;
}

use crate::payload::Payload;

// NOT SUPPORTED: rebootor
// TODO: open with wait

#[derive(Debug)]
pub struct Model {
    pub version: &'static str,
    pub mcu: &'static str,
    pub code_size: usize,
    pub block_size: usize,
    pub flash_offset: usize,
}

static MODELS: [Model; 7] = [
    // Teensy1, // {"at90usb162",   15872,   128}
    // Teensy1PP, // {"at90usb646",   64512,   256}
    // Teensy2, // {"atmega32u4",   32256,   128}
    // Teensy2PP, // {"at90usb1286", 130048,   256}
    Model {
        version: "LC",
        mcu: "mkl26z64",
        code_size: 63488,
        block_size: 512,
        flash_offset: 0,
    },
    Model {
        version: "3.0",
        mcu: "mk20dx128",
        code_size: 131072,
        block_size: 1024,
        flash_offset: 0,
    },
    Model {
        version: "3.1",
        mcu: "mk20dx256",
        code_size: 262144,
        block_size: 1024,
        flash_offset: 0,
    },
    Model {
        version: "3.2",
        mcu: "mk20dx256",
        code_size: 262144,
        block_size: 1024,
        flash_offset: 0,
    },
    Model {
        version: "3.5",
        mcu: "mk64fx512",
        code_size: 524288,
        block_size: 1024,
        flash_offset: 0,
    },
    Model {
        version: "3.6",
        mcu: "mk66fx1m0",
        code_size: 1048576,
        block_size: 1024,
        flash_offset: 0,
    },
    Model {
        version: "4.0",
        mcu: "imxrt1062",
        code_size: 2031616,
        block_size: 1024,
        flash_offset: 0x60000000,
    },
];

impl fmt::Display for Model {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Teensy {} ({}, code size: {}, block size: {})",
            self.version, self.mcu, self.code_size, self.block_size
        )
    }
}

impl Model {
    pub fn get(version: &str) -> Option<&'static Self> {
        let lc_v = version.to_lowercase();
        MODELS
            .iter()
            .find(|m| m.version.to_lowercase() == lc_v || m.mcu.to_lowercase() == lc_v)
    }

    pub fn iter() -> &'static [Self] {
        &MODELS
    }

    fn write_size(&self) -> usize {
        self.block_size + 64
    }
}

pub struct Teensy {
    dev: sys::Device,
    model: &'static Model,
    buf: Vec<u8>,
}

impl Teensy {
    pub fn open(model: &'static Model, wait: bool) -> Fallible<Self> {
        let mut dev = sys::Device::open()?;
        while let None = dev {
            if wait {
                std::thread::sleep(Duration::from_millis(100));
                dev = sys::Device::open()?;
            } else {
                return Err(err_msg("Teensy not found"));
            }
        }
        let dev = dev.unwrap(); // This unwrap will not panic due to the wait loop above

        trace!("Teensy open");
        Ok(Self {
            dev,
            model,
            buf: vec![0u8; model.write_size()],
        })
    }

    /// Timeout for writing a block
    const BLOCK_WRITE_TIMEOUT: Duration = Duration::from_millis(500);

    /// Bigger timeout for writing the first block
    const FIRST_BLOCK_WRITE_TIMEOUT: Duration = Duration::from_secs(5);

    fn write_block(&mut self, addr: usize, data: &[u8]) -> Fallible<usize> {
        trace!("Write block @{:x} {:?}", addr, data);
        if data.len() > self.model.block_size {
            bail!(
                "Data is {} bytes long, which is more than block size ({})",
                data.len(),
                self.model.block_size
            );
        }

        self.buf[0..size_of::<usize>()].copy_from_slice(&addr.to_le_bytes());
        self.buf[64..data.len() + 64].copy_from_slice(data);
        for b in self.buf[data.len() + 64..].iter_mut() {
            *b = 0;
        }

        // TODO: original code retries until timeout passes
        let timeout = if addr == 0 {
            Self::FIRST_BLOCK_WRITE_TIMEOUT
        } else {
            Self::BLOCK_WRITE_TIMEOUT
        };
        Ok(self.dev.write_block(&self.buf, timeout)?)
    }

    /// Flash a Payload instance to Teensy
    pub fn write_payload(&mut self, payload: &Payload) -> Fallible<()> {
        for (addr, data) in payload.iter_by_block(self.model.block_size, self.model.flash_offset) {
            self.write_block(addr, &data)?;
            std::thread::sleep(Duration::from_millis(10));
        }
        Ok(())
    }

    /// Reboot into uploaded code
    pub fn boot(mut self) -> Fallible<()> {
        // let mut payload = vec![0xFFu8, 0xFFu8, 0xFFu8];
        // payload.resize(self.model.write_size(), 0);
        // self.write(&payload, Duration::from_millis(500)).map(|_| ())
        self.write_block(0xFFFFFF, &[]).map(|_| ())
    }
}
