use std::path::Path;

use elf;
use elf::types::{
    ELFCLASS32, ELFOSABI_NONE, EM_ARM, ET_EXEC, PT_DYNAMIC, PT_INTERP, SHF_ALLOC, SHT_PROGBITS,
};
use failure::{ensure, format_err, Fallible};
use log::{debug, trace, warn};

struct Section<'a> {
    shdr: &'a elf::types::SectionHeader,
    data: &'a Vec<u8>,
    load_addr: u64,
}

impl<'a, 'b> Section<'a> {
    fn new(section: &'a elf::Section, phdrs: &'b Vec<elf::types::ProgramHeader>) -> Self {
        if let Some(phdr) = phdrs.iter().find(|phdr| {
            section.shdr.addr >= phdr.vaddr
                && section.shdr.addr + section.shdr.size <= phdr.vaddr + phdr.memsz
        }) {
            Self {
                shdr: &section.shdr,
                data: &section.data,
                load_addr: section.shdr.addr - phdr.vaddr + phdr.paddr,
            }
        } else {
            warn!("No load addr found for section: {}", section);
            Self {
                shdr: &section.shdr,
                data: &section.data,
                load_addr: section.shdr.addr,
            }
        }
    }
}

impl std::fmt::Display for Section<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} (load addr: 0x{:x})", self.shdr, self.load_addr)
    }
}

pub struct Payload {
    pub base_addr: usize,
    pub data: Vec<Option<u8>>,
}

impl Payload {
    /// Load an ELF file as a payload.
    ///
    /// Includes sanity checks: the input needs to be 32-bit ARM
    /// executable, "NONE" OS ABI, statically linked. Possibly more
    /// sanity checks will be introduced in the future.
    pub fn read_elf_file<P: AsRef<Path>>(path: P) -> Fallible<Self> {
        let file = elf::File::open_path(&path)
            // we need to .map_err() because elf::Error doesn't implement std error
            .map_err(|err| format_err!("Parsing input as ELF file: {:?}", err))?;

        trace!("{}", file.ehdr);

        // Sanity checks on the input file
        ensure!(
            file.ehdr.class == ELFCLASS32,
            "Input file is not 32-bit: {}",
            file.ehdr.class
        );

        ensure!(
            file.ehdr.machine == EM_ARM,
            "Input file is not ARM: {}",
            file.ehdr.machine
        );

        ensure!(
            file.ehdr.osabi == ELFOSABI_NONE,
            "Input file ABI is not NONE: {}",
            file.ehdr.osabi
        );

        ensure!(
            file.ehdr.elftype == ET_EXEC,
            "Input file is not an executable: {}",
            file.ehdr.elftype
        );

        for phdr in file.phdrs.iter() {
            trace!("{}", phdr);
            ensure!(
                phdr.progtype != PT_DYNAMIC && phdr.progtype != PT_INTERP,
                "Input file is not statically linked"
            );
        }

        // sort sections by address
        let sections: Vec<Section> = file
            .sections
            .iter()
            .filter(|s| s.shdr.shtype == SHT_PROGBITS && s.shdr.flags.0 & SHF_ALLOC.0 != 0)
            .map(|s| Section::new(s, &file.phdrs))
            .collect();

        ensure!(!sections.is_empty(), "No useful sections found");

        // We assume that sections don't overlap, as required by ELF
        // spec (I believe). We explicitly don't deal with malicious
        // input.

        let base_addr = sections.iter().map(|s| s.load_addr as usize).min().unwrap();
        let end_addr = sections
            .iter()
            .map(|s| (s.load_addr + s.shdr.size) as usize)
            .max()
            .unwrap();
        let size = end_addr - base_addr;
        debug!(
            "Data: {} (0x{:x}) bytes, 0x{:x} -- 0x{:x}",
            size, size, base_addr, end_addr
        );

        let mut data: Vec<Option<u8>> = vec![None; size];

        for section in sections {
            let start = section.load_addr as usize - base_addr;
            let end = start + section.shdr.size as usize;
            trace!("Section {} (0x{:x} -- 0x{:x})", section, start, end);
            data[start..end].copy_from_slice(
                &section
                    .data
                    .iter()
                    .map({ |v| Some(*v) })
                    .collect::<Vec<Option<u8>>>(),
            );
        }

        Ok(Self { base_addr, data })
    }

    /// Return a raw binary, with empty spaces between sections filled with zeroes.
    pub fn to_bin(&self) -> Vec<u8> {
        self.data.iter().map(|ob| ob.unwrap_or(0)).collect()
    }

    pub fn iter_by_block(
        &self,
        block_size: usize,
        flash_offset: usize,
    ) -> impl Iterator<Item = (usize, Vec<u8>)> + '_ {
        let start = self.base_addr - flash_offset;

        if start % block_size != 0 {
            // TODO: handle unaligned start?
            panic!("Start address is not block-aligned, I wasn't prepared for that!");
        }

        // Always prepend "block zero", take it if block zero is
        // already started. This is stupid extra work, but otherwise I
        // cannot return the same type in both cases without
        // implementing a new iterator (which I should do at some
        // point).
        let mut iter = std::iter::once((0, vec![0u8; block_size])).chain(
            self.data
                .chunks(block_size)
                .enumerate()
                .filter(|(_i, block)| block.iter().any(|o| o.is_some()))
                .map(move |(i, block)| {
                    let mut filled_block =
                        block.iter().map(|ob| ob.unwrap_or(0)).collect::<Vec<u8>>();
                    if filled_block.len() < block_size {
                        filled_block.resize(block_size, 0);
                    }
                    (start + i * block_size, filled_block)
                }),
        );

        // pop the fake first block off the iterator if it's already
        // included in the payload
        if start < block_size {
            iter.next();
        }

        iter
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: test reading an ELF file? Get a library of fixtures?

    #[test]
    fn test_to_bin_base_0() {
        let payload = Payload {
            base_addr: 0,
            data: vec![Some(1), Some(2), None, None, Some(3)],
        };
        assert_eq!(payload.to_bin(), vec![1, 2, 0, 0, 3]);
    }

    #[test]
    fn test_to_bin_base_non0() {
        let payload = Payload {
            base_addr: 23,
            data: vec![Some(1), Some(2), None, None, Some(3)],
        };
        assert_eq!(payload.to_bin(), vec![1, 2, 0, 0, 3]);
    }

    #[test]
    fn test_iter_by_block_fake_first_sector() {
        let payload = Payload {
            base_addr: 64,
            data: vec![
                Some(1),
                Some(2),
                None,
                None,
                None,
                None,
                None,
                None,
                Some(3),
                Some(4),
                Some(5),
                Some(6),
            ],
        };
        assert_eq!(
            payload
                .iter_by_block(4, 0)
                .collect::<Vec<(usize, Vec<u8>)>>(),
            vec![
                (0, vec![0u8; 4]),
                (64, vec![1u8, 2u8, 0u8, 0u8]),
                (72, vec![3u8, 4u8, 5u8, 6u8])
            ]
        );
    }

    #[test]
    fn test_iter_by_block_real_first_sector() {
        let payload = Payload {
            base_addr: 0,
            data: vec![
                Some(1),
                Some(2),
                None,
                None,
                None,
                None,
                None,
                None,
                Some(3),
                Some(4),
                Some(5),
                Some(6),
            ],
        };
        assert_eq!(
            payload
                .iter_by_block(4, 0)
                .collect::<Vec<(usize, Vec<u8>)>>(),
            vec![(0, vec![1u8, 2u8, 0u8, 0u8]), (8, vec![3u8, 4u8, 5u8, 6u8])]
        );
    }

    #[test]
    fn test_iter_by_block_offset() {
        let payload = Payload {
            base_addr: 65536,
            data: vec![
                Some(1),
                Some(2),
                None,
                None,
                None,
                None,
                None,
                None,
                Some(3),
                Some(4),
                Some(5),
                Some(6),
            ],
        };
        assert_eq!(
            payload
                .iter_by_block(4, 65536)
                .collect::<Vec<(usize, Vec<u8>)>>(),
            vec![(0, vec![1u8, 2u8, 0u8, 0u8]), (8, vec![3u8, 4u8, 5u8, 6u8])]
        );
    }
}
