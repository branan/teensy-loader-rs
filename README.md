Teensy Loader, in Rust
======================

This is a reimplementation of [Teensy loader
CLI](https://www.pjrc.com/teensy/loader_cli.html) in Rust.

Usage
-----

    cargo install teensy-loader
    teensy-loader --help

Pros
----

 - Not written in C
 - Accepts ELF binary as input (no need to `objconv` to an ihex file)
 - Supports Teensy 4.0 (not tested yet)

Cons
----

 - Not official, possibly buggy (new project)
 - Does not accept ihex input, so it might not fit into existing
   toolchains
 - Works only with libusb (Linux, FreeBSD; tested only on the latter)

I'll accept merge requests that fix the last two cons, as long as they
don't break existing functionality. Not going to implement these
features, though: what I need is uploading from ELF on FreeBSD.

Future
------

 - A cargo plugin that would make it possible to run
   `cargo teensy-load` in a Rust project aimed at a Teensy and it
   would Just Work (taking Teensy model from `Cargo.toml` or
   command-line switch).
